package com.example.phase1;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

public class SplashScreen extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		String restoredText = preferences.getString("isFirstTime", "no");
		Log.i("reeeeeestore", restoredText);
		Intent i = null;
		if (restoredText.equals("no")) {
			i = new Intent(SplashScreen.this, MainActivity.class);

		} else {
			i = new Intent(SplashScreen.this, home.class);

		}
		startActivity(i);
		finish();
	}

}
