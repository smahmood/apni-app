package com.example.phase1;


import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class feeddataadapter extends ArrayAdapter<feeddata>{
	Context context;
	ArrayList<feeddata> values;
	DrawableManager drawableManager;
	ImageView imageView;
	
	public feeddataadapter(Context context,ArrayList<feeddata> values){
		super(context, R.layout.feeddata, values);
		 drawableManager=new DrawableManager();
		this.context = context;
		this.values = values;
		drawableManager=new DrawableManager();
	}
	
	public View getView(int position,View ConvertView,ViewGroup parent){
		View row;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		row = inflater.inflate(R.layout.feeddata, parent,false);
		ImageView imageview = (ImageView)row.findViewById(R.id.icon2);
		imageview.setImageResource(values.get(position).getImageId());
		TextView textview2 = (TextView)row.findViewById(R.id.second_line1);
		String text=" is going to ";
		text+=values.get(position).getplace();
		
		text+=values.get(position).getwith();
		Log.e("testttttttttttttttttttttttttt", values.get(position).getid());
		imageView=(ImageView)row.findViewById(R.id.icon2);
		drawableManager.fetchDrawableOnThread("http://demo.exprodes.com/images/"+values.get(position).getuserimage(),imageView);
		Log.e("testttttttttttttttttttttttttt", values.get(position).getid());
		
		textview2.setText(values.get(position).getname());
		TextView textview3 = (TextView)row.findViewById(R.id.fourth_line1);
		textview3.setText(text);
		TextView textview4 = (TextView)row.findViewById(R.id.third_line1);
		textview4.setText(values.get(position).gettime());
		ImageButton b1=(ImageButton)row.findViewById(R.id.icon3);
		b1.setTag(values.get(position).getid());
		textview2.setTag(values.get(position).getuserid());
		
		return row;
	}
}
