package com.example.phase1;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

	public DataBaseHelper(Context context) {
		super(context,"db",null,3);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
				String Create_Table_Users = "CREATE TABLE Users(id INTEGER PRIMARY KEY,phoneNo text)";
				
				db.execSQL(Create_Table_Users);
				
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}
	public long setPhoneNo (String phoneNo) {
		long id=0;
		
		SQLiteDatabase db=getWritableDatabase();
		ContentValues values=new ContentValues();
		
		values.put("phoneNo", phoneNo);
		
		id=db.insert("Users", null, values);
		
		return id;
		
	}
	public String getPhoneNo() {
		SQLiteDatabase db=getReadableDatabase();
		Cursor cursor=db.rawQuery("select phoneNo from Users", null);
		int columnNo=cursor.getColumnIndexOrThrow("phoneNo");
		cursor.moveToFirst();
		String phone= cursor.getString(columnNo);
		return phone;
		
	}
	public feeddata[] getAllFeeds(int userid) {
		SQLiteDatabase db=getReadableDatabase();
		ArrayList<feeddata> feeddatas=new ArrayList<feeddata>();
		String sql="select * from ((FriendList join Attendees on friendid=attendeeid) join Events on id=eventid) where userid="+userid+" GROUP BY eventid ";
		Cursor cursor=db.rawQuery(sql, null);
		
		Cursor cursor2=db.rawQuery("select username from Users where userid="+userid,null);
		cursor2.moveToFirst();
		String name=cursor2.getString(0);
		
		if(cursor.moveToFirst())
		{
			int eventidColumnNo=cursor.getColumnIndexOrThrow("eventid");
			int currEventid=cursor.getInt(eventidColumnNo);
			int friendIdColumnNo=cursor.getColumnIndexOrThrow("friendid");
			int placeColumnNo=cursor.getColumnIndexOrThrow("place");
			String withField=null;
			String place=null;
			do {
				if (cursor.getInt(eventidColumnNo)!=currEventid) {
					//feeddatas.add(new feeddata(0,name, place, withField));
					withField=null;
				}
				withField=withField+cursor.getString(friendIdColumnNo)+" ";
				place=cursor.getString(placeColumnNo);
			} while (cursor.moveToNext());
		}
		
		return (feeddata[])feeddatas.toArray();
	}
	
	public long createUser(String username,String password) {
		long id=0;
		
		SQLiteDatabase db=getWritableDatabase();
		ContentValues values=new ContentValues();
		
		values.put("username", username);
		values.put("password", password);
		
		id=db.insert("Users", null, values);
		
		return id;
	}
	@SuppressLint("NewApi")
	public boolean authenticate(String username,String password) {
		SQLiteDatabase db=getReadableDatabase();
		Cursor cursor=db.query(true, "Users", new String[]{"username","password"}, "username=? and password=?",new String[] {username,password}, null, null, null, null, null);
		return cursor.moveToNext();
	}
	
	public long createEvent(String type,String location,String organizer) {
		long id=0;
		
		SQLiteDatabase db=getWritableDatabase();
		ContentValues values=new ContentValues();
		
		values.put("organizer",organizer);
		values.put("place", location);
		
		id=db.insert("Events", null, values);
		return id;
	}
	
	public long enterAntendee(int eventid,int attendeeid ) {
			long id=0;
			
			SQLiteDatabase db=getWritableDatabase();
			ContentValues values=new ContentValues();
			
			values.put("eventid", eventid);
			values.put("attendeeid", attendeeid);
			
			id=db.insert("Attendeed", null, values);
			
			return id;
	}
	
	
	
	

}
