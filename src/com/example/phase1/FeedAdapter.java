package com.example.phase1;

import java.util.List;

import com.example.phase1.data.RssItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class FeedAdapter extends ArrayAdapter<RssItem>{
	
	Context context;
	int lay_id;
	List<RssItem> data;

	public FeedAdapter(Context context, int resource, List<RssItem> data) {
		super(context, resource, data);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.lay_id = resource;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View row;
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		row = inflater.inflate(lay_id, parent, false);
		
		TextView tv1 = (TextView)row.findViewById(R.id.title_id);
		tv1.setText(data.get(position).getTitle().toString());
		
		TextView tv2 = (TextView)row.findViewById(R.id.link_id);
		tv2.setText(data.get(position).getDescription().toString());
		
		return row;
	}

}
