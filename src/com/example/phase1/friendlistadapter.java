package com.example.phase1;
import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class friendlistadapter extends ArrayAdapter<friendlist> {
	Context context;
	ArrayList<friendlist> values;
	DrawableManager drawableManager;
	public friendlistadapter(Context context,ArrayList<friendlist> values){
		super(context, R.layout.friendlist, values);
		this.context = context;
		this.values = values;
		drawableManager=new DrawableManager();
	}
	
	public View getView(int position,View ConvertView,ViewGroup parent){
		View row;
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		row = inflater.inflate(R.layout.friendlist, parent,false);
		TextView textview2 = (TextView)row.findViewById(R.id.second_line);
		textview2.setText(values.get(position).getname());
		textview2.setTag(values.get(position).getuser_id());
		ImageView imageview = (ImageView)row.findViewById(R.id.icon);
		imageview.setTag(values.get(position).getuser_id());
		//imageview.setImageResource(values.get(position).getImageId());
		
		drawableManager.fetchDrawableOnThread("http://demo.exprodes.com/images/"+values.get(position).getImageId(),imageview);
		
		
		
		return row;
	}
	public void profile(View view) {
		//	String a=(String) view.getTag();
			
		//	Intent intent = new Intent(this,profile.class);
		//	intent.putExtra("userid", a);
		//	startActivity(intent);
			
			
		}
		
}
