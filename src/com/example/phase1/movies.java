package com.example.phase1;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.kapati.widgets.DatePicker;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.google.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.GoogleMapOptionsCreator;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import com.google.android.gms.maps.MapFragment;
 
	
public class movies extends Activity implements OnMapClickListener {
	String movieLocation;
	String movieName;
	String movieDate;
	String shareOption;
	String regStart;
	String regEnd; 
	String phoneNo;
	String type;
	EditText editText;
	int selectedId;
	RadioButton radioButton;
	RadioGroup radioGroup;
	
	List<Address> possibleLocations;
	Geocoder gc;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies);
        gc=new Geocoder(this);
        editText=(EditText)findViewById(R.id.name);
        radioGroup=(RadioGroup)findViewById(R.id.radioType);
        ((DatePicker)findViewById(R.id.moviedate_text)).setDateFormat(DateFormat.getDateFormat(this));
		editText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				selectedId = radioGroup.getCheckedRadioButtonId();
			    radioButton = (RadioButton) findViewById(selectedId);
			    String tString = radioButton.getText().toString();
			    if("Movie".equals(tString)){
			    	Intent i = new Intent(getApplicationContext(), FeedsClass.class);
					startActivityForResult(i, 1);
			    }
			}
		});

       // AdView adView = (AdView)this.findViewById(R.id.adView);
		//adView.loadAd(new AdRequest());
    }
    public void showMap(View view)
    {
    	Dialog dialog=new Dialog(this);
    	dialog.setContentView(R.layout.mapdailog);
    	dialog.setTitle("Set Location");
    	dialog.show();
    	//GoogleMapOptions options=new GoogleMapOptions();
    	//options.zoomControlsEnabled(true);
    	//options.
    	//MapView mapView=(MapView)findViewById(R.id.map);
    	GoogleMap map=((MapFragment) getFragmentManager().findFragmentById(R.id.map))
    	        .getMap();
    	map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
    	//builder.setView(mapView);
    	map.setOnMapClickListener(this);
    	
    	
    }
    public void onMapClick(LatLng arg0) {
		try {
			possibleLocations=gc.getFromLocation(arg0.latitude, arg0.longitude, 3);
			EditText editText2=(EditText)findViewById(R.id.movielocation_text);
			if (possibleLocations.size()>0) {
				editText2.setText(possibleLocations.get(0).getLocality());
			}
	    	
		} catch (IOException e) {
			 //TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    public void post(View view) {
    	//phoneNo=new DataBaseHelper(getApplicationContext()).getPhoneNo();
    	
    	selectedId = radioGroup.getCheckedRadioButtonId();
	    radioButton = (RadioButton) findViewById(selectedId);
    	
    	
		EditText editText2=(EditText)findViewById(R.id.movielocation_text);
		EditText editText3=(EditText)findViewById(R.id.moviedate_text);
		CheckBox chkBox_f=(CheckBox)findViewById(R.id.checkBox1);
		CheckBox chkBox_p=(CheckBox)findViewById(R.id.checkBox2);
		
		movieName=editText.getText().toString();
		movieLocation=editText2.getText().toString();
		movieDate=editText3.getText().toString();
		type=radioButton.getText().toString();
		//this.regStart="2013-12-10";
		//this.regEnd="2013-12-10";
		boolean a=true;
		if (movieName.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "Name is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
			a=false;
		}
		else if (this.regStart.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "Registration start date is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
			a=false;
		}
		else if (movieLocation.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "Location is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
			a=false;
		}
		else if (movieLocation.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "Location is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
			a=false;
		}
		else if (movieDate.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "Date is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
			a=false;
		}
		if (chkBox_f.isChecked() && chkBox_p.isChecked()) {
			shareOption="3";
			
		}
		else if (chkBox_f.isChecked()) {
			shareOption="1";
			
		}
		else if (chkBox_p.isChecked()) {
			shareOption="2";
			
		}
		else if(!(chkBox_f.isChecked() || chkBox_p.isChecked())){
			Toast toast=Toast.makeText(getApplicationContext(), "Event have to be shared with someone", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
			a=false;
		}
		if (a) {
			movieLocation=editText2.getText().toString();
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(this);
			phoneNo = preferences.getString("isFirstTime", "yes");
			String url="http://www.demo.exprodes.com/post.php";
			new PostTask().execute(url);
			Intent retIntent = new Intent(this,SocialPage.class);
			startActivity(retIntent);
		}
		
		

	}
    
    private class PostTask extends AsyncTask<String, String, String>
	{
		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(String... params) {
			String urlString=params[0];
			DefaultHttpClient httpclient = new DefaultHttpClient();
			//DataBaseHelper dBaseHelper=new DataBaseHelper(getApplicationContext());
			HttpPost httppost = new HttpPost(
			"http://www.demo.exprodes.com/movies.php");
			

			try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("name", movieName));
			nameValuePairs.add(new BasicNameValuePair("location", movieLocation));
			nameValuePairs.add(new BasicNameValuePair("date", movieDate));
			nameValuePairs.add(new BasicNameValuePair("sharewith", shareOption));
			nameValuePairs.add(new BasicNameValuePair("regstart", regStart));
			nameValuePairs.add(new BasicNameValuePair("regend", regEnd));
			nameValuePairs.add(new BasicNameValuePair("type", type));
			nameValuePairs.add(new BasicNameValuePair("phoneNo",phoneNo));
		

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			//int status = response.getStatusLine().getStatusCode();
			//BufferedReader reader = new BufferedReader(
			//new InputStreamReader(response.getEntity().getContent()));
			//String abc = (reader.readLine());
			//JSONObject jb = new JSONObject(abc);
			//JSONArray finalResult = jb.getJSONArray("data");
			} catch (ClientProtocolException e) {

			 
			} catch (IOException e) {


			}
			
			
			return null;
		}
		
	}
    
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
    	if(requestCode == 1){
    		
    		if(resultCode == RESULT_OK){
    			editText.setText(data.getStringExtra("name"));
    		}
    		if(resultCode == RESULT_CANCELED){
    			
    		}
    		
    	}
    }
   

}