package com.example.phase1;

import java.util.List;

import com.example.phase1.data.RssItem;
import com.example.phase1.util.RssReader;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FeedsClass extends Activity{

	FeedAdapter adpAdapter;
	ListView lv;
	RssReader rssReader;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.feed_layout);
		
		new PostTask().execute("abc");
	}
	
	private class PostTask extends AsyncTask<String,Integer, String>{
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
	
		@Override
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				rssReader = new RssReader("http://www.movies.com/rss-feeds/top-ten-box-office-rss");
				lv = (ListView)findViewById(R.id.listView1);
				final List<RssItem> abcItems = rssReader.getItems();
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						adpAdapter = new FeedAdapter(FeedsClass.this, R.layout.feed_row_layout,abcItems);
						lv.setAdapter(adpAdapter);
						lv.setOnItemClickListener(new OnItemClickListener() {

							@Override
							public void onItemClick(AdapterView<?> arg0,
									View view, int pos, long id) {
								// TODO Auto-generated method stub
								Intent intent = new Intent();
								intent.putExtra("name", abcItems.get(pos).getTitle());
								setResult(RESULT_OK, intent);
								finish();
							}
							
						});
					}
				});
				
			} catch (Exception e) {
				// TODO: handle exception
				Log.e("RssReader", e.getMessage());
			}
			return null;
		}
	}

	

}
