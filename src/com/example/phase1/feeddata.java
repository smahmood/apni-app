package com.example.phase1;

public class feeddata{
	
	int img_id;
	String name;
	String place;
	String with;
	String time;
	String id;
	String userimage;
	String userid;
	
	public feeddata(int img_id,String name,String place,String with,String time,String id,String userimage,String userid){
		this.img_id = img_id;
		this.name = name;
		this.place=place;
		this.with=with;
		this.time=time;
		this.id=id;
		this.userimage=userimage;
		this.userid=userid;
		
	}
	
	public String getname(){
		return name;
	}
	
	
	public int getImageId(){
		return img_id;
	}
	public String getplace(){
		return place;
	}
	public String getwith(){
		return with;
	}
	public String gettime(){
		return time;
	}
	public String getid(){
		return id;
	}
	public String getuserimage(){
		return userimage;
	}
	public String getuserid(){
		return userid;
	}
	

}
