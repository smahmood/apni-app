package com.example.phase1;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import winterwell.jtwitter.Twitter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;

public class SocialPage extends FragmentActivity implements OnClickListener{
	
	private FacebookFragment mainFragment;
	
	private static final String TAG = "OAuthDemo";
	  private static final String OAUTH_KEY = "YOUR_KEY_GOES_HERE";
	  private static final String OAUTH_SECRET = "YOUR_SECRET_GOES_HERE";
	  private static final String OAUTH_CALLBACK_SCHEME = "x-marakana-oauth-twitter";
	  private static final String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME
	      + "://callback";
	  private static final String TWITTER_USER = "YOUR_EMAIL_GOES_HERE";

	  
	  private OAuthConsumer mConsumer;
	  private OAuthProvider mProvider;
	  private Twitter twitter;
	  SharedPreferences prefs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null) {
	        // Add the fragment on initial activity setup
	        mainFragment = new FacebookFragment();
	        getSupportFragmentManager().beginTransaction().add(android.R.id.content, mainFragment).commit();
	    } else {
	        // Or set the fragment from restored state info
	        mainFragment = (FacebookFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
	    }
	   
	}
	
	
	public void goback(View v){
		Intent intent = new Intent(this,home.class);
		startActivity(intent);
	}


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}

}
