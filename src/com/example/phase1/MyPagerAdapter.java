package com.example.phase1;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyPagerAdapter extends FragmentPagerAdapter{
	
	private String[] TITLES;
	private String dumy_data;

	public MyPagerAdapter(FragmentManager fm, String[] tiles,String dumy_data) {
		super(fm);
		this.TITLES = tiles;
		this.dumy_data = dumy_data;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLES[position];
	}

	@Override
	public int getCount() {
		return TITLES.length;
	}

	@Override
	public Fragment getItem(int position) {
		return SuperAwesomeCardFragment.newInstance(position,dumy_data);
	}
}
