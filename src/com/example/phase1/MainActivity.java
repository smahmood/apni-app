package com.example.phase1;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

import com.google.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.facebook.*;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		try{ Log.w("abc", "Checking signs");
	    PackageInfo info = getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_SIGNATURES);
	    for (Signature signature : info.signatures) {
	        MessageDigest md = MessageDigest.getInstance("SHA");
	        md.update(signature.toByteArray());
	        Log.w("abc",Base64.encodeToString(md.digest(), Base64.DEFAULT));
	    }
	} catch (NameNotFoundException e) {
	    e.printStackTrace();
	    Log.w("abc",e.getMessage());
	} catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	    Log.w("abc",e.getMessage());
	}
		
		
		AdView adView = (AdView)this.findViewById(R.id.adView);
		//adView.loadAd(new AdRequest());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	public void signup(View view)
	{
		DataBaseHelper dBaseHelper=new DataBaseHelper(getApplicationContext());
		EditText editText[]=new EditText[5];
		editText[0]=(EditText)findViewById(R.id.phoneNo);
		/*editText[1]=(EditText)findViewById(R.id.editText2);
		editText[2]=(EditText)findViewById(R.id.editText3);
		editText[3]=(EditText)findViewById(R.id.editText4);
		editText[4]=(EditText)findViewById(R.id.editText5);*/
		
		String username=editText[0].getText().toString();
		//String password=editText[2].getText().toString();
		//dBaseHelper.createUser(username, password);
		//boolean chk=dBaseHelper.authenticate(username, password);
		
		
		
		/*if(len==0)
		{
			Toast toast=new Toast(MainActivity.this);
			toast.setText("Name is Required");
			toast.setGravity(Gravity.CENTER,0,0);
			toast.setDuration(Toast.LENGTH_LONG);
			toast.show();
		}*/
		//else 
		{
			String actionName= "android.intent.action.home";
			
			Intent intent = new Intent(actionName);
			startActivity(intent);
		}
		
	}

	public void personalinfo(View view)
	{
		EditText editText=(EditText)findViewById(R.id.phoneNo);
		String phoneNo=editText.getText().toString();
		if(phoneNo.length()!=0)
		{
			String actionName= "android.intent.action.personalinfo";
			Intent intent = new Intent(actionName);
			intent.putExtra("phoneNo", phoneNo);
			startActivity(intent);
			finish();
		}
		else {
			Toast toast=Toast.makeText(getApplicationContext(), "Phone No. is required", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0,200);
			toast.show();
		}
		
	}

}
