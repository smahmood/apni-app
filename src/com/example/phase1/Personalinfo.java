package com.example.phase1;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import net.kapati.widgets.DatePicker;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.android.gms.ads.AdView;



public class Personalinfo extends Activity {
	public String ba1="";
	private String phoneNo;
	String outPut;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.personalinfo);
		
		((DatePicker)findViewById(R.id.datePicker1)).setDateFormat(DateFormat.getDateFormat(this));
		
		Intent intent=getIntent();
		Bundle bundle=intent.getExtras();
		
		phoneNo=bundle.getString("phoneNo");
		//new  HttpUploader().execute(" ");
		
		
		AdView adView = (AdView)this.findViewById(R.id.adView);
		//adView.loadAd(new AdRequest());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void upload(View view)
	{
		Intent intent = new Intent();
		  intent.setType("image/*");
		  intent.setAction(Intent.ACTION_GET_CONTENT);
		  startActivityForResult(Intent.createChooser(intent, "Select Picture"),1);
		  
		  
		  
	}
	@SuppressWarnings("deprecation")
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
			if (resultCode == RESULT_OK) {
				if (requestCode == 1) {
				// currImageURI is the global variable I�m using to hold the content:
					Uri currImageURI = data.getData();
					String [] proj={MediaStore.Images.Media.DATA};
					android.database.Cursor cursor = managedQuery( currImageURI,
					proj,     // Which columns to return
					null,     // WHERE clause; which rows to return (all rows)
					null,     // WHERE clause selection arguments (none)
					null);     // Order-by clause (ascending by name)
					int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
					cursor.moveToFirst();
					Button b1=(Button)findViewById(R.id.main_custom_button);
					b1.setText("Wait While Picture is being Uploaded");
		            b1.setEnabled(false);
					new  HttpUploader().execute(cursor.getString(column_index));
					
					ImageButton ib=(ImageButton)findViewById(R.id.imageButton1);
					Bitmap myBitmap = BitmapFactory.decodeFile(cursor.getString(column_index));
					ib.setImageBitmap(myBitmap);
					
					
					
					
					
				}
			}
			
		}
	
	//////////////////////
	
		/////////////////////////////
	public void personalinfo(View view)
	{
		EditText name=(EditText)findViewById(R.id.name_e_p);
		String nameString=name.getText().toString();
		EditText date = (EditText)this.findViewById(R.id.datePicker1);
		String dateString=date.getText().toString();
		EditText sign=(EditText)findViewById(R.id.sign_e_p);
		String signString=sign.getText().toString();
		
		if(nameString.length()==0)
		{
			Toast toast=Toast.makeText(getApplicationContext(), "Name is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
		}
		else if(dateString.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "DOB is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
		}
		else if(signString.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "Zodiac Sign is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
		}
		else {
			String actionName= "android.intent.action.interest";
			Intent intent = new Intent(actionName);
			intent.putExtra("phoneNo", phoneNo);
			intent.putExtra("Name", nameString);
			intent.putExtra("DOB", dateString);
			intent.putExtra("Sign", signString);
			intent.putExtra("Image",outPut);
			startActivity(intent);
			finish();
			
			
		}
		
	}
	
	private class HttpUploader  extends AsyncTask<String, String, String>
	{
		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}
		
		@Override
		 protected String doInBackground(String... path) {
			outPut = null;
            
            
            for (String sdPath : path) {
             
                Bitmap bitmapOrg = BitmapFactory.decodeFile(sdPath);
                ByteArrayOutputStream bao = new ByteArrayOutputStream();
                 
                //Resize the image
                double width = bitmapOrg.getWidth();
                double height = bitmapOrg.getHeight();
                double ratio = 400/width;
                int newheight = (int)(ratio*height);
                 
                System.out.println("���-width" + width);
                System.out.println("���-height" + height);
                 
                bitmapOrg = Bitmap.createScaledBitmap(bitmapOrg, 400, newheight, true);
                 
                //Here you can define .PNG as well
                bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 95, bao);
                byte[] ba = bao.toByteArray();
                String ba1 = Base64.encodeToString(ba, 0);
                 
                
                 
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("image", ba1));
                 
                try {
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost("http://demo.exprodes.com/imageupload.php");
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                     
                    HttpResponse response = httpclient.execute(httppost);
                    HttpEntity entity = response.getEntity();                
 
                    // print responce
                    outPut = EntityUtils.toString(entity);
                    Log.i("GET RESPONSE�-", outPut);
                    System.out.println(outPut);
                     
                    //is = entity.getContent();
                    Log.e("log_tag ******", "good connection");
                     
                    bitmapOrg.recycle();
                     
                } catch (Exception e) {
                    Log.e("log_tag ******", "Error in http connection " + e.toString());
                }
            }
            return outPut;
			
        } 
		@Override
		protected void onPostExecute(String result) {
		   
			Button b1=(Button)findViewById(R.id.main_custom_button);
			b1.setText("Next");
            b1.setEnabled(true);
		}
		
	}
	
	
}