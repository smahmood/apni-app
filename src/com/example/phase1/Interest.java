package com.example.phase1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.security.auth.PrivateCredentialPermission;
 
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
 
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
public class Interest extends Activity {

	String name;
	String dob;
	String profession;
	String interest;
	String sign;
	String phoneNo;
	String image;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.interest);

		

		// intent.putExtra("phoneNo", phoneNo);
		// intent.putExtra("Name", nameString);
		// intent.putExtra("DOB", dateString);
		// intent.putExtra("Sign", signString);
		

		AdView adView = (AdView) this.findViewById(R.id.adView);
		//adView.loadAd(new AdRequest());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void personalinfo(View view) {

		
		Log.i("helooooooooooooooooo", "hogya");
		EditText interesti=(EditText)findViewById(R.id.interest);
		EditText professioni=(EditText)findViewById(R.id.profession);
		
		interest=interesti.getText().toString();
		profession=professioni.getText().toString();
		
		if (interest.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "Interest is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
		}
		else if (profession.length()==0) {
			Toast toast=Toast.makeText(getApplicationContext(), "Profession is required", Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 8);
			toast.show();
		}
		else {
			Intent intent = getIntent();
			Bundle bundle = intent.getExtras();
			phoneNo = bundle.getString("phoneNo");
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(this);
			SharedPreferences.Editor editor = preferences.edit();
			editor.putString("isFirstTime", phoneNo);
			
			editor.commit();
			DataBaseHelper dBaseHelper=new DataBaseHelper(getApplicationContext());
			dBaseHelper.setPhoneNo(phoneNo);
			name = bundle.getString("Name");
			dob = bundle.getString("DOB");
			sign = bundle.getString("Sign");
			image=bundle.getString("Image");
			String url="http://www.demo.exprodes.com/post.php";
			new PostTask().execute(url);
			
			Intent i = new Intent(Interest.this, home.class);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
			
		}
	}
	
	private class PostTask extends AsyncTask<String, String, String>
	{
		@Override
		protected void onPreExecute()
		{
			super.onPreExecute();
		}
		
		@Override
		protected String doInBackground(String... params) {
			String urlString=params[0];
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
			"http://www.demo.exprodes.com/post.php");
			try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6);
			nameValuePairs.add(new BasicNameValuePair("phoneNo", phoneNo));
			nameValuePairs.add(new BasicNameValuePair("sign", sign));
			nameValuePairs.add(new BasicNameValuePair("dob", dob));
			nameValuePairs.add(new BasicNameValuePair("name", name));
			nameValuePairs.add(new BasicNameValuePair("interest", interest));
			nameValuePairs.add(new BasicNameValuePair("profession", profession));
			nameValuePairs.add(new BasicNameValuePair("image", image));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			//int status = response.getStatusLine().getStatusCode();
			//BufferedReader reader = new BufferedReader(
			//new InputStreamReader(response.getEntity().getContent()));
			//String abc = (reader.readLine());
			//JSONObject jb = new JSONObject(abc);
			//JSONArray finalResult = jb.getJSONArray("data");
			} catch (ClientProtocolException e) {

			 
			} catch (IOException e) {


			}
			
			
			return null;
		}
		
	}
	
	

}
