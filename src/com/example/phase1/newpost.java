package com.example.phase1;

import com.google.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
 
@SuppressWarnings("deprecation")
public class newpost extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpost);
         
        TabHost tabHost = getTabHost();
         
        // Tab for Photos
        TabSpec photospec = tabHost.newTabSpec("Movies");
        // setting Title and Icon for the Tab
        photospec.setIndicator("New Event", getResources().getDrawable(R.drawable.movies));
        Intent photosIntent = new Intent(this, movies.class);
        photospec.setContent(photosIntent);
         
         
         
       
         
        // Adding all TabSpec to TabHost
        tabHost.addTab(photospec); // Adding photos tab
        
    }
}