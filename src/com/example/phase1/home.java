package com.example.phase1;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.R.integer;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class home extends FragmentActivity {
	
	private final Handler handler = new Handler();
	
	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private MyPagerAdapter adapter;

	private Drawable oldBackground = null;
	private int currentColor = 0xFF666666;
	String dumy_data ;
	String a;
	Cursor cur;
	Double lat,lon;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		pager = (ViewPager) findViewById(R.id.pager);
		String[] tab_titles = {"Feed","Friends"};
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		
		dumy_data = preferences.getString("isFirstTime", "yes");
		
		adapter = new MyPagerAdapter(getSupportFragmentManager(),tab_titles,dumy_data);
		
		pager.setAdapter(adapter);
		
		/*final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
				.getDisplayMetrics());
		pager.setPageMargin(pageMargin);
*/
		tabs.setViewPager(pager);
		
		 ContentResolver cr = getContentResolver();
	        cur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null,null, null);
	     
	     new Send().execute(" ");
	     LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
	     Criteria criteria = new Criteria();
	     String bestProvider = locationManager.getBestProvider(criteria, false);
	     Location location = locationManager.getLastKnownLocation(bestProvider);
	     
	     try {
	       lat = location.getLatitude ();
	       lon = location.getLongitude ();
	       int a=2;
	      }
	     catch (NullPointerException e){
	         e.printStackTrace();
	      
	     }
	     new location().execute(" ");

	          
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    @Override
    protected void onResume() {
    	// TODO Auto-generated method stub
    	super.onResume();
    	int isAvailable = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            //return true;
        } else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(isAvailable,
                    this,10);
            dialog.show();
        } else {
            Toast.makeText(this, "Connect Connect to Maps", Toast.LENGTH_SHORT)
                    .show();

        }
    }
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case R.id.action_newpost:
			Intent i = new Intent(this,newpost.class);
			startActivity(i);
			return true;
		case R.id.peoplearound:
			Intent ii = new Intent(this,peoplearound.class);
			startActivity(ii);
			return true;

		}

		return super.onOptionsItemSelected(item);
	}
	
	public void upload(View view) {
		 a=(String) view.getTag();
		 new join().execute(" ");
		 view.setVisibility(View.GONE);
		
		
	}
	public void profile(View view) {
		String a=(String) view.getTag();
		
		Intent intent = new Intent(this,profile.class);
		intent.putExtra("userid", a);
		startActivity(intent);
		
		
	}
	
	public class Send extends AsyncTask<String,String, String>{
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
	
		@Override
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
		    
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
			"http://www.demo.exprodes.com/contacts.php");

			try {
			int i=0;
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			while (cur.moveToNext()) {

	            String name =cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));    
	            String phoneNumber = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	            
	            nameValuePairs.add(new BasicNameValuePair("contact"+i, phoneNumber));
				//httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	            i++;
	            
	            }
			nameValuePairs.add(new BasicNameValuePair("total", String.valueOf(i)));
			//httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			nameValuePairs.add(new BasicNameValuePair("userid",dumy_data));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			int status = response.getStatusLine().getStatusCode();
			BufferedReader reader = new BufferedReader(
			new InputStreamReader(response.getEntity().getContent()));
			
			} catch (ClientProtocolException e) {

				Log.e("Tag", "Could not get HTML: " + e.getMessage());
			}
		catch (IOException e) {
				Log.e("Tag", "Could not ge: " + e.getMessage());

			}

			
		return params[0];
				
			}
			
		}
			
	public class location extends AsyncTask<String,String, String>{
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
	
		@Override
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
		    
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
			"http://www.demo.exprodes.com/location.php");

			try {
			int i=0;
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			nameValuePairs.add(new BasicNameValuePair("userid",dumy_data));
			nameValuePairs.add(new BasicNameValuePair("lat",String.valueOf(lat)));
			nameValuePairs.add(new BasicNameValuePair("lon",String.valueOf(lon)));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			int status = response.getStatusLine().getStatusCode();
			BufferedReader reader = new BufferedReader(
			new InputStreamReader(response.getEntity().getContent()));
			
			} catch (ClientProtocolException e) {

				Log.e("Tag", "Could not get HTML: " + e.getMessage());
			}
		catch (IOException e) {
				Log.e("Tag", "Could not ge: " + e.getMessage());

			}

			
		return params[0];
				
			}
			
		}
	
	
	public class join extends AsyncTask<String,String, String>{
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
	
		@Override
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
		    
			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(
			"http://www.demo.exprodes.com/join.php");

			try {
			int i=0;
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			
			
			nameValuePairs.add(new BasicNameValuePair("userid",dumy_data));
			nameValuePairs.add(new BasicNameValuePair("eventid",a));
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			
			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			int status = response.getStatusLine().getStatusCode();
			
			
			} catch (ClientProtocolException e) {

				Log.e("Tag", "Could not get HTML: " + e.getMessage());
			}
		catch (IOException e) {
				Log.e("Tag", "Could not ge: " + e.getMessage());

			}

			
		return params[0];
				
			}
		@Override
		protected void onPostExecute(String result) {
			
		}
			
		}
	
	
	
	
	
	
	
}
