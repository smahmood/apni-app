package com.example.phase1;

public class friendlist{
	
	String img_id;
	String name;
	String userid;
	
	public friendlist(String img_id,String name,String userid){
		this.img_id = img_id;
		this.name = name;
		this.userid=userid;
		
	}
	
	public String getname(){
		return name;
	}
	public String getuser_id(){
		return userid;
	}
	
	public String getImageId(){
		return img_id;
	}

}
