package com.example.phase1;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import net.kapati.widgets.DatePicker;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phase1.SuperAwesomeCardFragment.PostTask;
import com.google.ads.AdRequest;
import com.google.android.gms.ads.AdView;



public class profile extends Activity {
	
	BufferedReader reader,reader1;
	ArrayList<feeddata> values;
	DrawableManager drawableManager;
	String userid;
	String[] arr,arr1;
	String myid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile);
			Intent i=getIntent();
			drawableManager=new DrawableManager();
			Bundle b=i.getExtras();
			userid=b.getString("userid");
		 
	        new Fetch().execute("abc");
	        SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(this);
			
			myid = preferences.getString("isFirstTime", "yes");
	        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public class Fetch extends AsyncTask<String,String, String>{
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
	
		@Override
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
		    
				DefaultHttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
				"http://www.demo.exprodes.com/profile.php");

				try {
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("userid", userid));
				nameValuePairs.add(new BasicNameValuePair("myid", myid));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				int status = response.getStatusLine().getStatusCode();
				reader = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
				
				} catch (ClientProtocolException e) {

					Log.e("Tag", "Could not get HTML: " + e.getMessage());
				}
			catch (IOException e) {
					Log.e("Tag", "Could not ge: " + e.getMessage());

				}

				
			return params[0];
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			
			String abc=null;
			try {
				abc = (reader.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			arr=abc.split(",");
				
			
			for(int j=0;j<Integer.parseInt(arr[0]);j++) {
				TextView t1=(TextView)findViewById(R.id.textView1);
				t1.setText(arr[1]);
				TextView t2=(TextView)findViewById(R.id.textView2);
				t2.setText("DOB: "+arr[2]);

				TextView t3=(TextView)findViewById(R.id.textView3);
				t3.setText(arr[3]);
				

				TextView t4=(TextView)findViewById(R.id.textView4);
				t4.setText("Interest : "+arr[4]);
				

				TextView t5=(TextView)findViewById(R.id.textView5);
				t5.setText("Profession : "+arr[5]);
				
				Button b1=(Button)findViewById(R.id.button1pr);
				b1.setVisibility(1);
				if(myid.equals(userid))
				{
					b1.setText("Edit Info");
					b1.setTag("1");
					
				}
				else
				{
					if(arr[10].equals("0"))
					{
						b1.setText("Add Friend");
						b1.setTag("2");
					}else
					{
						b1.setText("Friends");
						b1.setTag("3");
						b1.setEnabled(false);
						
					}
				}
				
				ProgressBar p=(ProgressBar)findViewById(R.id.progressBar1);
				p.setVisibility(View.INVISIBLE);
				
			
				ImageView imageView=(ImageView)findViewById(R.id.gundoga);
				drawableManager.fetchDrawableOnThread("http://demo.exprodes.com/images/"+arr[7],imageView);
				imageView.setVisibility(0);
				imageView.setScaleType(ScaleType.FIT_XY);
			}
			
		}
			
			
		}
	public void add(View view) {
		String a=(String) view.getTag();
		if(a.equals("1"))
		{
			
			String actionName= "android.intent.action.personalinfo";
			Intent intent = new Intent(actionName);
			intent.putExtra("phoneNo", myid);
			startActivity(intent);
			finish();
		}
		if(a.equals("2"))
		{ 
			new addfrnd().execute("abc");
		}
		
		
	}
	
	
	public class addfrnd extends AsyncTask<String,String, String>{
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
	
		@Override
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
		    
				DefaultHttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
				"http://www.demo.exprodes.com/addfrnd.php");

				try {
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("userid", userid));
				nameValuePairs.add(new BasicNameValuePair("myid", myid));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				int status = response.getStatusLine().getStatusCode();
				reader = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
				
				} catch (ClientProtocolException e) {

					Log.e("Tag", "Could not get HTML: " + e.getMessage());
				}
			catch (IOException e) {
					Log.e("Tag", "Could not ge: " + e.getMessage());

				}

				
			return params[0];
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Button b1=(Button)findViewById(R.id.button1pr);
				b1.setVisibility(View.GONE);
				
			}
			
		
			
			
		}

	
	
	
}
	

	
	
