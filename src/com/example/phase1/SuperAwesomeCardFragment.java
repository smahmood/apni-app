

package com.example.phase1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import com.example.phase1.data.RssItem;
import com.example.phase1.util.RssReader;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;

public class SuperAwesomeCardFragment extends Fragment {
	
	View fragmentview;
	View fragmentview2;
	SuperAwesomeCardFragment context;
	FrameLayout fl;
	FrameLayout fl2;
	ArrayList<feeddata> values;
	ArrayList<friendlist> values1;
	BufferedReader reader,reader1;
	String[] arr,arr1;
	private static final String ARG_POSITION = "position";
	ListView lv;

	private int position;
	public String dumy_data;

	public static SuperAwesomeCardFragment newInstance(int position,String dumy_data) {
		SuperAwesomeCardFragment f = new SuperAwesomeCardFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		b.putString("dumy_value", dumy_data);
		f.setArguments(b);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		position = getArguments().getInt(ARG_POSITION);
		context = SuperAwesomeCardFragment.this;
		dumy_data = getArguments().getString("dumy_value");
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build(); 
		StrictMode.setThreadPolicy(policy);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		FrameLayout ret_f = null;
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		if("0".equals(Integer.toString(position))){
			fragmentview = inflater.inflate(R.layout.feed, container, false);
			fl = new FrameLayout(getActivity());
			fl.setLayoutParams(params);
			ret_f = fl;
			lv =(ListView) fragmentview.findViewById(R.id.list_view2);
			}
		else{
			
			fragmentview2 = inflater.inflate(R.layout.friends, container, false);
			fl2 = new FrameLayout(getActivity());
			fl2.setLayoutParams(params);
			ret_f = fl2;
		}
		
		new PostTask().execute(Integer.toString(position));
		return ret_f;
	}
	
	public class PostTask extends AsyncTask<String,String, String>{
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}
	
		@Override
		public String doInBackground(String... params) {
			// TODO Auto-generated method stub
		    
				DefaultHttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
				"http://www.demo.exprodes.com/feed.php");

				try {
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("name", "test"));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				
				// Execute HTTP Post Request
				HttpResponse response = httpclient.execute(httppost);
				int status = response.getStatusLine().getStatusCode();
				reader = new BufferedReader(
				new InputStreamReader(response.getEntity().getContent()));
				
				} catch (ClientProtocolException e) {

					Log.e("Tag", "Could not get HTML: " + e.getMessage());
				}
			catch (IOException e) {
					Log.e("Tag", "Could not ge: " + e.getMessage());

				}

				
				
				
				
		///////////////////////////////////////////////////////////////
				DefaultHttpClient httpclient1 = new DefaultHttpClient();
				HttpPost httppost1 = new HttpPost(
				"http://www.demo.exprodes.com/friends.php");

				try {
				
				List<NameValuePair> nameValuePairs1 = new ArrayList<NameValuePair>();
				nameValuePairs1.add(new BasicNameValuePair("phone", dumy_data));
				httppost1.setEntity(new UrlEncodedFormEntity(nameValuePairs1));
				
				// Execute HTTP Post Request
				HttpResponse response1 = httpclient1.execute(httppost1);
				int status = response1.getStatusLine().getStatusCode();
				reader1 = new BufferedReader(
				new InputStreamReader(response1.getEntity().getContent()));
				
				} catch (ClientProtocolException e) {

					Log.e("Tag", "Could not get HTML: " + e.getMessage());
				}
			catch (IOException e) {
					Log.e("Tag", "Could not ge: " + e.getMessage());

				}

				/////////////////////////////////////////////////////
				
			return params[0];
		}
		
		@Override
		protected void onPostExecute(String result) {
			
			values=new ArrayList<feeddata>();
			String abc=null;
			try {
				abc = (reader.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			arr=abc.split(",");
				Log.i("testtttttt",arr[0] );
			int i=1;
			for(int j=0;j<Integer.parseInt(arr[0]);j++) {
				
				if (arr[0+i].equals(""))
				{
					if(Integer.parseInt(arr[5+i])==1) {
						values.add(new feeddata(R.drawable.glyphicons_003_user,arr[1+i]," watch "+arr[4+i]+" at "+arr[i+2],arr[0+i],arr[3+i],arr[i+6],arr[i+8],arr[i+7]));	
					}
					else {
						values.add(new feeddata(R.drawable.glyphicons_003_user,arr[1+i],arr[4+i],arr[0+i],arr[3+i],arr[i+6],arr[i+8],arr[i+7]));
					}
						
				}
				else {
					
					if(Integer.parseInt(arr[5+i])==1) {
						values.add(new feeddata(R.drawable.glyphicons_003_user,arr[1+i]," watch "+arr[4+i]+" at "+arr[i+2]," with"+arr[0+i],arr[3+i],arr[i+6],arr[i+8],arr[i+7]));
					}
					else {
						values.add(new feeddata(R.drawable.glyphicons_003_user,arr[1+i],arr[4+i]," with"+arr[0+i],arr[3+i],arr[i+6],arr[i+8],arr[i+7]));
					}
				}
					
			
				i=i+9;
			}
			
			if("0".equals(result)){
				feeddataadapter arrayadp = new feeddataadapter(getActivity(),values);
		    	lv.setAdapter(arrayadp);
		    	
		    	getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						fl.addView(lv);
					}
				});
			}
			else{
				values1=new ArrayList<friendlist>();
				abc=null;
				try {
					abc = (reader1.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				arr1=abc.split(",");
				 i=1;
				for(int j=0;j<Integer.parseInt(arr1[0]);j++) {
					values1.add(new friendlist(arr1[2+i+j],arr1[i+j],arr1[i+1+j]));
					i+=2;
				
				}
				
				final ListView lv2 =(ListView) fragmentview2.findViewById(R.id.list_view123);
				friendlistadapter arraya = new friendlistadapter(getActivity(),values1);
		    	lv2.setAdapter(arraya);
		    	
		    	getActivity().runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						fl2.addView(lv2);
					}
				});
			}
			
			
		}
	}

}